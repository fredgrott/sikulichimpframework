package org.bitbucket.fredgrott.sikulichimpframework.resolver;

/**
 * @author rohit
 *
 */
public interface IImageResolver {

	public String getImagePath(String imageName);
}